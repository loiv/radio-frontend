// @ts-nocheck

import { expect, test } from "@playwright/test";

import { DateTime } from "luxon";
import path from "path";

test.beforeEach(async ({ context }) => {
  await context.addInitScript({
    path: path.join(__dirname, "..", "./node_modules/sinon/pkg/sinon.js")
  });
  await context.addInitScript(() => {
    const datetimeToMock = new Date(2022, 2, 23, 11, 7, 18);
    var clock = sinon.useFakeTimers({
      now: datetimeToMock.valueOf(),
      toFake: ["Date"]
    });
  });
});

test("check track playing time binded to the value in HTML", async ({
  page,
  baseURL
}) => {
  let requestData;

  await page.route(`${baseURL}/api/v1/tracks/add`, (route, request) => {
    requestData = request.postDataJSON();
    void route.abort();
  });

  await page.goto("/add");
  await page
    .locator("#track-url")
    .fill("https://www.youtube.com/watch?v=2XeSQVWleqY");

  await page.locator("button[type=submit]").click();
  const playDatetimeFromForm = DateTime.fromISO(requestData["play-datetime"]);

  expect(
    playDatetimeFromForm.toISOTime() === "11:15:00.000+01:00"
  ).toBeTruthy();
});
