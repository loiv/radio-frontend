import { expect, test } from "@playwright/test";
test("get an appropriate playlist info at certain day when queue empty", async ({
  page,
  baseURL
}) => {
  await page.route(`${baseURL}/api/v1/queue/list*`, route => {
    void route.fulfill({
      status: 200,
      contentType: "application/json",
      body: "[]",
      headers: {
        "Access-Control-Allow-Origin": baseURL
      }
    });
  });
  await page.goto("/queue");
  await page.fill('input[type="date"]', "2022-01-17");

  await expect(page.locator(".tracks-fetch-result").first()).toContainText(
    "Brak utworów"
  );
});
