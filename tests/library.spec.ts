import { expect, test } from "@playwright/test";
test("check offset parameter value in request url when offset changes", async ({
  page,
  baseURL
}) => {
  let requestUrl = "";

  await page.route(`${baseURL}/api/v1/tracks/list*`, route => {
    void route.fulfill({
      body: JSON.stringify({
        count: 121,
        hasNext: true,
        success: true,
        tracks: [
          {
            duration: 208,
            status: "zaakceptowane",
            title: "AC/DC - Highway to Hell (Official Video)",
            trackUrl: "https://www.youtube.com/watch?v=l482T0yNkeo"
          },
          {
            duration: 250,
            status: "zaakceptowane",
            title: "AURORA - Runaway",
            trackUrl: "https://www.youtube.com/watch?v=d_HlPboLRL8"
          },
          {
            duration: 173,
            status: "zaakceptowane",
            title: "AVAION, VIZE, Leony - Pieces (Official Video)",
            trackUrl: "https://www.youtube.com/watch?v=mMbGQkvxVag"
          },
          {
            duration: 322,
            status: "zaakceptowane",
            title: "Afromental - Rock&Rollin'Love [Official Music Video]",
            trackUrl: "https://www.youtube.com/watch?v=0O7Xfb5dPSw"
          }
        ]
      })
    });
  });

  await page.goto("/library");
  await page.locator("#increment-offset-btn").click();

  await page.route(`${baseURL}/api/v1/tracks/list*`, (route, request) => {
    requestUrl = request.url();
    void route.abort();
  });
  await page.locator("#increment-offset-btn").click();

  const searchedOffsetInUrl = new URL(requestUrl).searchParams.get("offset");
  expect(parseInt(searchedOffsetInUrl) === 2).toBeTruthy();
});
