import {
  adminAuthenticatedMessage,
  trackErrorFixedInfoSuccess,
  trackManagePlayingResponse,
  trackManageSuccess,
  trackStartPlayingSuccess,
  trackStopPlayingSuccess
} from "./stores";
import {
  baseUrl,
  breaksTimes,
  musicFixedUri,
  musicStartUri,
  musicStopUri,
  queueDurationAPIUrl,
  userDefaultTimezone
} from "./constants";

import { DateTime } from "luxon";
import { checkProperlyAuthUri } from "./constants";

export interface FetchWithTimeoutOpts {
  timeout?: number;
  method?: string;
  headers?: Headers;
  body?: string;
  credentials?: RequestCredentials;
}

export interface TrackInfo {
  trackUrl: string;
  duration: number;
  title: string;
}

export async function fetchWithTimeout(
  resource: string | URL,
  options: FetchWithTimeoutOpts = { timeout: 7000 }
): Promise<Response> {
  const timeout = options?.timeout || 7000;

  // https://dmitripavlutin.com/timeout-fetch-request/
  const controller = new AbortController();

  const id = setTimeout(() => controller.abort(), timeout);

  const response = await fetch(resource.toString(), {
    ...options,
    signal: controller.signal
  });

  clearTimeout(id);

  return response;
}

export async function fetchData(
  url: string | URL,
  params: FetchWithTimeoutOpts = {}
): Promise<[Response | null, string]> {
  try {
    if (!params.credentials) {
      params.credentials = "include";
    }
    const response = await fetchWithTimeout(url, params);
    return [response, ""];
  } catch (error) {
    let errorMessage: string;

    if (error instanceof Error) {
      if (error.name === "AbortError") {
        errorMessage = "Przekroczono limit czasu żądania";
      } else if (error.name === "TypeError") {
        errorMessage = "Połączenie z serwerem nie powiodło się";
      } else {
        errorMessage = String(error);
      }
    } else {
      errorMessage = String(error);
    }
    return [null, errorMessage];
  }
}

export async function getResponseAsJson(
  response: Response
): Promise<[object, string]> {
  try {
    if (response instanceof Response) {
      return [await response.json(), ""];
    }
  } catch (error) {
    let errorMessage: string;

    if (error instanceof Error) {
      if (error.name === "SyntaxError") {
        errorMessage = "Błędny format odpowiedzi serwera";
      } else {
        errorMessage = String(error);
      }
    } else {
      errorMessage = String(error);
    }
    return [null, errorMessage];
  }
}

export async function fetchJsonResponse(
  url: string | URL,
  params: FetchWithTimeoutOpts
): Promise<[object, number, string]> {
  const [response, responseErrors] = await fetchData(url, params);
  if (response) {
    const [jsonData, jsonErrors] = await getResponseAsJson(response);
    if (jsonData) {
      return [jsonData, response.status, ""];
    } else {
      return [null, response.status, jsonErrors];
    }
  } else {
    return [null, -1, responseErrors];
  }
}

export async function deleteTrackFromQueue(
  url: string,
  trackUrl: string,
  queuedDatetime: string
): Promise<Response> {
  const fullUrl = new URL(url, baseUrl).toString();
  const params = {
    "track-url": trackUrl,
    "queued-datetime": queuedDatetime
  };
  return fetchWithTimeout(fullUrl, {
    method: "DELETE",
    body: JSON.stringify(params),
    timeout: 7000,
    headers: new Headers({
      "content-type": "application/json"
    })
  });
}
export async function manageTrack(
  url: string,
  trackUrl: string
): Promise<Response> {
  const fullUrl = new URL(url, baseUrl).toString();
  const params = {
    "track-url": trackUrl
  };
  return fetchWithTimeout(fullUrl, {
    method: "PATCH",
    body: JSON.stringify(params),
    timeout: 7000,
    headers: new Headers({
      "content-type": "application/json"
    })
  });
}

function getMessageBasedOnCaller(byCaller: boolean) {
  return byCaller
    ? "Akcja wykonana przez Ciebie"
    : "Akcja wykonana przez kogoś innego";
}

function updateTheActionCallerNotification(response) {
  if ("byCaller" in response && typeof response["byCaller"] === "boolean") {
    trackManagePlayingResponse.set(
      getMessageBasedOnCaller(response["byCaller"])
    );
  }
}

export async function informErrorHasBeenFixed() {
  const [response, , errorRespMessage] = await fetchJsonResponse(
    musicFixedUri,
    {
      method: "POST",
      timeout: 7000
    }
  );
  trackManagePlayingResponse.set("");
  if (
    response &&
    "success" in response &&
    typeof response["success"] === "boolean"
  ) {
    trackErrorFixedInfoSuccess.set(response["success"]);
    trackManageSuccess.set(response["success"]);
    if ("message" in response && typeof response["message"] === "string") {
      trackManagePlayingResponse.set(response["message"]);
    } else {
      updateTheActionCallerNotification(response);
    }
  } else {
    trackErrorFixedInfoSuccess.set(false);
    trackManagePlayingResponse.set(errorRespMessage);
  }
}
export async function stopPlayingMusic() {
  const [response, , errorRespMessage] = await fetchJsonResponse(musicStopUri, {
    method: "POST",
    timeout: 7000
  });
  trackManagePlayingResponse.set("");
  if (
    response &&
    "success" in response &&
    typeof response["success"] === "boolean"
  ) {
    trackStopPlayingSuccess.set(response["success"]);
    trackManageSuccess.set(response["success"]);
    if ("message" in response && typeof response["message"] === "string") {
      trackManagePlayingResponse.set(response["message"]);
    } else {
      updateTheActionCallerNotification(response);
    }
  } else {
    trackStopPlayingSuccess.set(false);
    trackManagePlayingResponse.set(errorRespMessage);
  }
}
export async function startPlayingMusic() {
  const [response, , errorRespMessage] = await fetchJsonResponse(
    musicStartUri,
    {
      method: "POST",
      timeout: 7000
    }
  );
  trackManagePlayingResponse.set("");
  if (
    response &&
    "success" in response &&
    typeof response["success"] === "boolean"
  ) {
    trackStartPlayingSuccess.set(response["success"]);
    trackManageSuccess.set(response["success"]);
    if ("message" in response && typeof response["message"] === "string") {
      trackManagePlayingResponse.set(response["message"]);
    } else {
      updateTheActionCallerNotification(response);
    }
  } else {
    trackStartPlayingSuccess.set(false);
    trackManagePlayingResponse.set(errorRespMessage);
  }
}
export async function checkIfAuthenticatedByServer(): Promise<boolean> {
  const [response, , errorMessage] = await fetchJsonResponse(
    checkProperlyAuthUri,
    { timeout: 5000 }
  );
  if (response) {
    if ("success" in response && typeof response["success"] === "boolean") {
      if ("message" in response && typeof response["message"] === "string") {
        adminAuthenticatedMessage.set(response["message"]);
      }
      return response["success"];
    } else {
      adminAuthenticatedMessage.set(
        "Nieznana przyczyna błędu uwierzytelnienia"
      );
      return false;
    }
  } else {
    adminAuthenticatedMessage.set(errorMessage);
    return false;
  }
}

export async function getLeftTimeDuringBreaksOn(
  choosenDate: string
): Promise<[object, number, string]> {
  const url = new URL(queueDurationAPIUrl, baseUrl);

  const params = { "play-date": choosenDate };

  url.search = new URLSearchParams(params).toString();
  return fetchJsonResponse(url, { timeout: 2000 });
}

export function constructPlayingDatetime(
  playingDate: string,
  playingTime: string
): string | null {
  const playDatetime = DateTime.fromISO(
    `${playingDate}T${playingTime}`
  ).setZone(userDefaultTimezone);

  const playISODatetime = playDatetime.toISO();
  return playISODatetime;
}

export function checkBreakIsOver(
  choosenDate: string,
  breakStartTimeStr: string
): boolean {
  const breakStartTime = DateTime.fromISO(breakStartTimeStr);
  if (!(breakStartTimeStr in breaksTimes)) {
    return true;
  }
  const breakDuration: number = breaksTimes[breakStartTimeStr];
  const breakStartDatetime = DateTime.fromISO(choosenDate, {
    zone: userDefaultTimezone
  }).set({
    hour: breakStartTime.hour,
    minute: breakStartTime.minute
  });
  const breakEndDatetime = breakStartDatetime.plus({
    minutes: breakDuration
  });
  if (breakEndDatetime < DateTime.local({ zone: userDefaultTimezone })) {
    return true;
  }
  return false;
}
