export const baseUrl = window.location.origin;

const apiPath = `${baseUrl}/api/v1`;
const authPath = `${baseUrl}/auth`;
const musicPath = `${baseUrl}/music`;

export const acceptTrackAPIUrl = `${apiPath}/tracks/accept`;
export const rejectTrackAPIUrl = `${apiPath}/tracks/reject`;
export const addTrackAPIUrl = `${apiPath}/tracks/add`;
export const listTracksAPIUrl = `${apiPath}/tracks/list`;

export const deleteTrackFromQueueAPIUrl = `${apiPath}/queue/track`;
export const listQueueAPIUrl = `${apiPath}/queue/list`;
export const queueDurationAPIUrl = `${apiPath}/queue/duration`;

export const listHistoryAPIUrl = `${apiPath}/history/list`;

export const loginUri = `${authPath}/token`;
export const checkProperlyAuthUri = `${authPath}/check`;

export const accountsUri = `${baseUrl}/accounts/`;

export const musicStartUri = `${musicPath}/start`;
export const musicStopUri = `${musicPath}/stop`;
export const musicStatusUri = `${musicPath}/status`;
export const musicStatusNotificationUri = `${musicPath}/status/notify`;
export const musicFixedUri = `${musicPath}/fixed`;

export const musicPlayerHealthUrl = `${musicPath}/player/health`;
export const backendServerHealthUrl = `${baseUrl}/health`;
export const notifierHealthUrl = `${musicStatusUri}/health`;

export const breaksTimes = {
  "08:30": 10,
  "09:25": 10,
  "10:20": 10,
  "11:15": 15,
  "12:15": 10,
  "13:10": 10,
  "14:05": 10,
  "15:00": 10
};

export const userDefaultTimezone = "Europe/Warsaw";
