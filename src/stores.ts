import { writable } from "svelte/store";

export const searchOffset = writable(0);

function createAdminCheck() {
  const { subscribe, set } = writable(false);
  return {
    subscribe,
    markAsAuthorized: () => set(true),
    reset: () => set(false)
  };
}
export const isAdmin = createAdminCheck();

function createCaptchaSolutionStore() {
  const { subscribe, set } = writable("");
  return {
    subscribe,
    setSolution: (solution: string) => set(solution),
    reset: () => set("")
  };
}
export const captchaSolution = createCaptchaSolutionStore();

export const historyPlayDate = writable("");
export const queuePlayDate = writable("");
export const trackUrl = writable("");
export const playDate = writable("");
export const playTime = writable("");

export const trackStatusAccepted = writable(true);
export const trackStatusPendingApprove = writable(true);
export const trackStatusRejected = writable(false);

export const adminAuthenticatedMessage = writable("");

export const trackManagePlayingResponse = writable("");
export const trackManageSuccess = writable(false);

export const trackStartPlayingSuccess = writable(false);
export const trackStopPlayingSuccess = writable(false);
export const trackErrorFixedInfoSuccess = writable(false);

export const playerManuallyStopped = writable(false);
export const playerErrorOccured = writable(false);
export const playerTrackTitle = writable("");

export const breakTimesAvailable = writable({});
