declare namespace svelte.JSX {
    interface HTMLAttributes<T> {
      gsiResp?: (event: any) => any;
      ongsiResp?: (event: any) => any;
      captchaUsed ?: (event: any) => any;
      oncaptchaUsed ?: (event: any) => any;
    }
  }