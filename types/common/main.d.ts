type NavbarUrls = {
    name: string;
    href: string;
    color: string;
    active: boolean;
  }[];