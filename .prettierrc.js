module.exports = {
  arrowParens: "avoid",
  singleQuote: false,
  printWidth: 80,
  semi: true,
  svelteSortOrder: "options-scripts-markup-styles",
  svelteStrictMode: false,
  svelteIndentScriptAndStyle: true,
  trailingComma: "none"
};
